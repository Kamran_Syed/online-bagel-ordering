<?php
/*
Plugin Name: Online Bagel Ordering.
Plugin URI: 
Description: Plugin Used For Online Ordering Of Bagel.
Author: Agile Solutions PK
Version: 1.1
Author URI: http://www.agilesolutionspk.com
*/

if ( !class_exists( 'Online_Bagel' )){
	class Online_Bagel{
		function __construct() {
			register_activation_hook( __FILE__, array($this, 'install') );
			add_action( 'admin_menu', array(&$this, 'admin_menu') );
		}
		
		function admin_menu(){
			
		}
	}
}
new Online_Bagel();
?>